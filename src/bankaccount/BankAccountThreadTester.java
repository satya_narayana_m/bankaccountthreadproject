/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bankaccount;

/**
 *
 * @author S521743
 */
public class BankAccountThreadTester  {
    public static void main(String[] args) throws InterruptedException{
         BankAccount account = new BankAccount();
final double AMOUNT = 100;
final int REPETITIONS = 1000;
DepositRunnable deposit=new DepositRunnable(account, AMOUNT, REPETITIONS);
WithdrawRunnable withdraw=new WithdrawRunnable(account, AMOUNT, REPETITIONS);
Thread depositThread=new Thread(deposit);
Thread withdrawThred=new Thread(withdraw);
depositThread.start();
withdrawThred.start();
    }
   
}
