/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bankaccount;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S521743
 */
public class DepositRunnable implements Runnable{
    private static final int DELAY = 10;
private BankAccount account;
private double amount;
private int count;

    public DepositRunnable(BankAccount account, double amount, int count) {
        this.account = account;
        this.amount = amount;
        this.count = count;
    }

    @Override
    public void run() {
        for (int i = 0; i < count; i++) {
            try {
                account.deposit(amount);
                Thread.sleep(DELAY);
            } catch (InterruptedException ex) {
                Logger.getLogger(DepositRunnable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
